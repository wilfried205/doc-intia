# Logiciel Intia

Ce logiciel intia est une application de gestion de clients et assurance réalisé avec Spring Boot (backend) et React (frontend).

## Prérequis

Avant de commencer, assurez-vous d'avoir installé les éléments suivants sur votre système :

- Java Development Kit (JDK) 17 ou version ultérieure
- Node.js avec npm (Node Package Manager)
- Git (pour cloner le dépôt)

## Installation

1. Clonez ce dépôt en utilisant la commande suivante :

 	 git clone https://gitlab.com/foningwilfried/intia.git


2. acceder au repertoire backend
 	cd INTIA

3. lancer l'application backend
	./mvnw spring-boot:run
4.Dans un autre terminal, accédez au répertoire frontend :
	cd intiafront

5. Installez les dépendances du frontend en exécutant la commande suivante :
	npm install
6.lancer l'application frontend
	npm start
Cela va démarrer l'application frontend et ouvrir une page dans votre navigateur par défaut à l'adresse http://localhost:3000. Vous pouvez maintenant utiliser l'application pour gérer les clients.




Déploiement
Pour déployer le logiciel intia dans un environnement de production, vous devrez suivre les étapes suivantes :

Générez les fichiers de construction pour le backend en exécutant la commande suivante dans le répertoire backend :
	./mvnw package
Cela générera un fichier JAR exécutable dans le répertoire target.
Copiez le fichier JAR généré dans votre environnement de production.
Assurez-vous que Java est installé sur votre environnement de production.
Exécutez le fichier JAR en utilisant la commande suivante :
	java -jar intia.jar

Cela lancera l'application backend sur le port par défaut 8080.
Pour le frontend, vous devez générer les fichiers optimisés pour la production en exécutant la commande suivante dans le répertoire frontend :
	npm run build

Cela générera les fichiers statiques dans le répertoire build.
Copiez les fichiers générés dans un serveur Web approprié pour servir les fichiers statiques.
Configurez le serveur Web pour rediriger toutes les requêtes non résolues vers le fichier index.html afin de permettre à React Router de gérer les routes.
Accédez à l'application en utilisant l'URL du serveur Web configuré.